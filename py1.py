
def add(a, b):
    """
    计算两个数的和
    :param a: 数字1
    :param b: 数字2
    :return: 两数之和
    """
    return a + b



import pytest
from allure_commons.types import Severity
from allure_commons.logger import AllureFileLogger
from allure_commons.utils import now
from allure_commons.utils import uuid4

def setup_module(module):
    print("开始测试")

def teardown_module(module):
    print("结束测试")

@pytest.mark.hebeu
@pytest.mark.parametrize("a, b, expected", [(1, 2, 3), (0, 0, 0), (-1, 1, 0)])
def test_add(a, b, expected):
    print("开始计算")
    assert add(a, b) == expected
    print("结束计算")

@pytest.mark.hebeu
@pytest.mark.parametrize("a, b, expected", [(1.1, 2.2, 3.3), (0.0, 0.0, 0.0), (-1.1, 1.1, 0.0)])
def test_add_float(a, b, expected):
    print("开始计算")
    assert add(a, b) == expected
    print("结束计算")

@pytest.mark.hebeu
@pytest.mark.parametrize("a, b, expected", [("1", "2", "12"), ("hello", "world", "helloworld"), ("1", "a", "1a")])
def test_add_str(a, b, expected):
    print("开始计算")
    assert add(a, b) == expected
    print("结束计算")

@pytest.mark.hebeu
@pytest.mark.parametrize("a, b, expected", [([1, 2], [3, 4], [1, 2, 3, 4]), ([], [], [])])
def test_add_list(a, b, expected):
    print("开始计算")
    assert add(a, b) == expected
    print("结束计算")

@pytest.mark.hebeu
@pytest.mark.parametrize("a, b, expected", [(1, "2", TypeError), ("hello", 2, TypeError)])
def test_add_exception(a, b, expected):
    print("开始计算")
    with pytest.raises(expected):
        add(a, b)
    print("结束计算")

@pytest.mark.hebeu
def test_add_allure():
    logger = AllureFileLogger(uuid4())
    logger.write("开始计算", now(), Severity.NORMAL)
    assert add(1, 2) == 3
    logger.write("结束计算", now(), Severity.NORMAL)
